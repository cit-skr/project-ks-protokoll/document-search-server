# document-search-server

Elasticsearch based document search engine with a Flask API

Downloads documents using links from hasura database, converts the scraped documents to plain text format, parses them into structured text format and then adds these as entries into the elasticsearch


## Tasks/Issues:
- remove repeated patterns: https://stackoverflow.com/questions/12468613/regex-to-remove-repeated-character-pattern-in-a-string
- fix error: struct error unpack requires a buffer of 38 bytes pdfminer struct.error
- fix error: pdfminer.pdfdocument.PDFTextExtractionNotAllowed: Text extraction is not allowed: <_io.BytesIO object at 0x7f2520633170>

## Features to add:
- add an elastic search index for search history
- api endpoint for sutosuggest based on search history
- Test out statistical text cleaning on pdfs
- move the filesystem to MongoDB
- [x] write a Dockerfile for the server
- one docker compose / docker swarm / kube config for all the containers
- 