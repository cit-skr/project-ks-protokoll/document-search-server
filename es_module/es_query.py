""" App configuration. """
import os, sys, dotenv, datetime, traceback, re
parent_folder_path = os.path.dirname( os.path.abspath(__file__)).split("document-search-server")[0]
sys.path.append(parent_folder_path)

# Find .env file
basedir = os.path.join(parent_folder_path,"document-search-server")
dotenv.load_dotenv(os.path.join(basedir, '.env'))

# General Config
NOW = datetime.datetime.now() 
FILE_DOWNLOAD_PATH = os.path.join(parent_folder_path, os.environ.get("FILE_DOWNLOAD_PATH"))
ELASTICSEARCH_HOST = os.environ.get("ELASTICSEARCH_HOST")
ELASTICSEARCH_PORT = os.environ.get("ELASTICSEARCH_PORT")
ELASTICSEARCH_INDEX_NAME = os.environ.get("ELASTICSEARCH_INDEX_NAME")
ELASTICSEARCH_nHits = os.environ.get("ELASTICSEARCH_nHits")
ELASTICSEARCH_nResults = os.environ.get("ELASTICSEARCH_nResults")

from elasticsearch import Elasticsearch
import json
import nltk
nltk.download('stopwords')
nltk.download('punkt')
from nltk import word_tokenize
from nltk.corpus import stopwords
import string
SWEDISH_STOP_WORDS = stopwords.words('swedish') + list(string.punctuation)

def get_es_client():
    global ELASTICSEARCH_HOST, ELASTICSEARCH_PORT
    client = Elasticsearch([{'host':ELASTICSEARCH_HOST,'port':ELASTICSEARCH_PORT}])
    return client

def clean_query(query):
    global SWEDISH_STOP_WORDS
    return ' '.join([i for i in word_tokenize(query.lower(), language='swedish') if i not in SWEDISH_STOP_WORDS])

def hasNumbers(inputString):
    return any(char.isdigit() for char in inputString)


def delete_document(client, path, size):
    try:
        delete_query = {
            'bool':{
                'must':[
                    {
                        "match": {
                            "path": path
                        }   
                    },
                    {
                        "match": {
                            "size": size
                        }   
                    },
                ]

            }
        }
        source_list = {"includes": ["title", "size", "date", "file_id", "municipality_id", "page_count", "url", "summary", "path"]}

        response = client.search(
            index= ELASTICSEARCH_INDEX_NAME,
            body={
                "query": delete_query,
                "_source": source_list
            }
        )

        results = response['hits']['hits']

        for doc_to_delete in results:
            delete_id = doc_to_delete.get('_id')
            delete_path = '/'.join(doc_to_delete.get("_source").get('path').replace(basedir,'').split('/')[:-1])

            if delete_path in path:
                del_response = client.delete(ELASTICSEARCH_INDEX_NAME,delete_id)
                jsonl_file = open(os.path.join(basedir,"delete_history.jsonl"),'a+')
                jsonl_file.write(json.dumps(doc_to_delete) + "\t" + json.dumps(del_response) +"\n")
                jsonl_file.close()

        return 0, response
    except BaseException as e:
        print(e)
        traceback.print_exc()
        return None, e

def find_duplicates(client):
    query =  {
            "duplicateCount": {
            "terms": {
                "field": "size",
                "min_doc_count": 2,
                "size": 10
            },
            "aggs": {
                "duplicateDocuments": {
                "top_hits": {
                    "size": 10
                }
                }
            }
            }
        }

    response = client.search(
        index= ELASTICSEARCH_INDEX_NAME,
        body={
            "size": 100,
            "aggs": query
         
        }
    )

    return response


def search(client, query, suggest=False, max_search_results=10, max_hits=10):

    global ELASTICSEARCH_INDEX_NAME
    try:
        query = clean_query(query)
        # print(query)
        split_query = query.strip().split(' ')
        multi_match = ''
        for word in split_query:
            if word.isupper() or word.isnumeric() or hasNumbers(word):
                multi_match += word+' '
            else:
                if len(word)>3:
                    multi_match += '*'+word+'* ' 
                else:
                    multi_match += word+' '

            
        nWords = len(split_query)

        suggestions = {
            # "text" : query,
            # "simple_phrase": {
            #     "phrase": {
            #         "field": "text",
            #         "size": 10,
            #         "gram_size": nWords+1,
            #         "direct_generator": [ {
            #             "field": "text",
            #             "suggest_mode": "missing",
            #             "sort":"frequency"
            #         },
            #         {
            #             "field" : "text",
            #             "suggest_mode" : "always"
            #         },
            #         {
            #             "field" : "text",
            #             "suggest_mode" : "popular"
            #         } ],
            #     }
            # }
            "simple_phrase": {
                "prefix": query,
                "completion": {
                    "field": "text.completion",
                    "fuzzy": {
                        "fuzziness": "AUTO"
                    }
                }
            }
            
        }

        mlt_query = {
            "more_like_this": {
                "fields": ["text"],
                "like": query,
                "min_term_freq": 1,
                "max_query_terms": 50,
                "min_doc_freq": 1
            }
        }
        

        simple_query = {
            'bool':{
                'must':[
                    {
                        "match_bool_prefix": {
                            "text": {
                                "query": query,
                            }
                        }
                    },
                
                ],
                'should':[
                    
                    {
                        "query_string": {
                            "query": multi_match,
                            "fields": ["text"],
                            "minimum_should_match":nWords-1
                        }
                    },
                    {
                        "prefix": {
                            "text": {
                                "value": query
                            }
                        }
                    },
                    # mlt_query
                ]
            }
            
        }

        sample = {
            "match":{
                "text":{"query": query}
            }
        }

        sort_search_results = [
            "_score",
            { "date" : {"order" : "desc"}}
            
        ]

        source_list = {"includes": ["title", "size", "date", "file_id", "municipality_id", "page_count", "url", "summary", "path"]}

        if suggest:
            response = client.search(
                index= ELASTICSEARCH_INDEX_NAME,
                body={
                    "sort": sort_search_results,
                    "size": max_search_results,
                    "query": simple_query,
                    "suggest": suggestions,
                    "_source": source_list ,
                }
            )
        else:
            response = client.search(
                index= ELASTICSEARCH_INDEX_NAME,
                body={
                    "sort": sort_search_results,
                    "size": max_search_results,
                    "query": simple_query,
                    "suggest": suggestions,
                    "_source": source_list,
                    "highlight" : {
                        "number_of_fragments" : max_hits,
                        "fragment_size" : 300,
                        "pre_tags" : ['<b>'] , 
                        "post_tags" : ["</b>"], 
                        "fields" : {
                            "text":{
                            }
                        }
                    }
                }
            )


        search_results = []
        if not suggest:
            for result in response['hits']['hits']:
                search_result = {
                    "score": result['_score'],
                    "title": result['_source']['title'],
                    "file_id": result['_source']['file_id'],
                    "path": result['_source']['path'],
                    "municipality_id": result['_source']['municipality_id'],
                    "date": result['_source']['date'],
                    "size": result['_source']['size'],
                    "url": result['_source']['url'],
                    "summary": result['_source']['summary'],
                    "page_count": result['_source']['page_count'],
                    "items": result['highlight']['text']
                }
                search_results.append(search_result)

        search_suggestions = []
        for suggestion in response['suggest']['simple_phrase']:
            for option in suggestion['options']:
                search_suggestions.append(option)
                
        return search_results, search_suggestions, "ok"
    except BaseException as e:
        return [], [], str(e)




def test_search():
    client = get_es_client()
    query = input("Enter query: ")
    search_results, suggestions, _ = search(client, query)
    print(query)
    print(json.dumps(search_results, indent=4))
    print(json.dumps(suggestions, indent=4))

if __name__=='__main__':
    try:
        while True:
            test_search()
    except KeyboardInterrupt as e:
        print(e)
        exit(0)
    except BaseException as e:
        print(e)
    # client = get_es_client()
    # # find_duplicates(client)
    # path = "/mnt/InternalStorage/sidkas/project_ks_protocoll/downloaded_pdfs/Östergötlands län/Motala/2021/2020-06-10.pdf"
    # size = 645421
    # delete_document(client, path, size)

