# FROM ubuntu:18.04

FROM python:3.6

ENV SHELL=/bin/bash

RUN apt-get update -y && apt-get install -y cmake protobuf-compiler

RUN mkdir /document-search-server
WORKDIR /document-search-server

COPY ./requirements.txt /document-search-server/requirements.txt


RUN pip install --upgrade pip
RUN pip install -r requirements.txt 
