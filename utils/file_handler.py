import os, sys, errno, io, re, json, requests, time, dotenv, datetime, traceback, shutil
parent_folder_path = os.path.dirname( os.path.abspath(__file__)).split("document-search-server")[0]
sys.path.append(parent_folder_path)

# Find .env file
basedir = os.path.join(parent_folder_path,"document-search-server")
dotenv.load_dotenv(os.path.join(basedir, '.env'))

# General Config
NOW = datetime.datetime.now() 
FILE_DOWNLOAD_PATH = os.path.join(parent_folder_path, os.environ.get("FILE_DOWNLOAD_PATH"))

from utils.common_tools import mkdir_p

def delete_file(path):
    global FILE_DOWNLOAD_PATH
    trash_path = os.path.join(FILE_DOWNLOAD_PATH, ".trash")
    mkdir_p(trash_path)
    
    try:
        if os.path.isdir(path):
            shutil.rmtree(path)
        else:
            # rootdir = FILE_DOWNLOAD_PATH.rstrip(os.sep)
            start = FILE_DOWNLOAD_PATH.rfind(os.sep) + 1
            relative_path = path[start:]
            folders = path[start:].split(os.sep)
            parent = str(trash_path)
            for f in folders[:-1]:
                current = os.path.join(parent,f)
                mkdir_p(current)
                parent = current
            shutil.move(path,os.path.join(trash_path,relative_path))
        return {"status": "success"}
    except BaseException as e:
        traceback.print_exc()
        print(e)
        return {"status": str(e)}

def rename_file_folder(path, new_name):
    try:
        new_name = new_name.split('.')[0]
        extension = '.' + path.split(".")[1] if len(path.split("."))>1 else ""
        directory = '/'.join(path.split('/')[:-1])
        new_file_path = os.path.join(directory,new_name+extension)
        shutil.move(path,new_file_path)
        return {"status": "success"}
    except BaseException as e:
        traceback.print_exc()
        return {"status": str(e)}


def create_new_folder(path, new_folder_name):
    try:
        mkdir_p(path, new_folder_name)
        return {"status": "success"}
    except BaseException as e:
        traceback.print_exc()
        return {"status": str(e)}

