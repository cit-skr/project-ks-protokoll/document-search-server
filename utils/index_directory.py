import os, json, time
from functools import reduce


ITEM_ID = 0

def sizeof_fmt(num, suffix='B'):
    for unit in ['','K','M','G','T','P','E','Z']:
        if abs(num) < 1024.0:
            return "%3.1f%s%s" % (num, unit, suffix)
        num /= 1024.0
    return "%.1f%s%s" % (num, 'Yi', suffix)

def get_id():
    global ITEM_ID
    ITEM_ID += 1
    return str(ITEM_ID)

def get_directory_structure(rootdir):
    """
    Creates a nested dictionary that represents the folder structure of rootdir
    """
    global ITEM_ID
    ITEM_ID = 0
    fg = {}
    rootdir = rootdir.rstrip(os.sep)
    start = rootdir.rfind(os.sep) + 1
    main_dir = rootdir[start:]
    for i,(path, dirs, files) in enumerate(os.walk(rootdir)):
        if "trash" not in path.lower():
            folders = path[start:].split(os.sep)
            subdir = {}
            for j,f in enumerate(files):
                file_path = os.path.join(path,f)
                subdir[f] = { "name": f, "path":file_path, "size": os.path.getsize(file_path), "__date": time.ctime(os.path.getctime(file_path))}
            parent = reduce(dict.get, folders[:-1], fg)
            subdir["path"] = path 
            parent[folders[-1]] = subdir
    return rees(fg[main_dir], main_dir, get_id())


def get_directory_file_counts(rootdir):
    import numpy as np
    import folderstats
    # import matplotlib.pyplot as plt
    df = folderstats.folderstats(rootdir, ignore_hidden=True)
    df_folders = df[df['folder']]
    regions = df_folders.loc[(df_folders.depth==1 )& (df_folders.name!="undefined") ]
    kommun  = df_folders.loc[df_folders.depth==2]

    print("Total pdf count:", len(df.loc[df.extension=='pdf']), ", regions:",len(regions),", kommuns:", len(kommun))

    region_hist = regions[['name','num_files']].plot(x='name',figsize=(15, 15),
        kind='bar', color='C1', title='region by file count').get_figure()
    region_hist.savefig("file_count_region.jpeg")

    kommun.loc[kommun['num_files'] > 800, 'num_files'] = 800
    kommun_hist = kommun[['name','num_files']].plot(x='name',figsize=(50, 30),
        kind='bar', color='C1', title='kommun by file count').get_figure()
    kommun_hist.savefig("file_count_kommun.jpeg")

    bad_data  = kommun.loc[kommun.num_files<15]
    bad_data[["name","num_files"]].to_csv("low_pdf_count.csv", index=False)
    return 0



def rees(df, key, _id):
    return { 
        "id": _id,
        "name": key,
        "path": df.pop("path",None) , 
        "size": df.pop("size",None) , 
        "date": df.get("__date",None) , 
        "children":[rees(df[key], key,get_id()) for key in list(df.keys())] if "__date" not in list(df.keys()) else None
    }


if __name__=='__main__':
    # print(get_df("/mnt/InternalStorage/sidkas/project_ks_protocoll/archives"))
    # print(json.dumps(get_directory_structure("/mnt/InternalStorage/sidkas/project_ks_protocoll/archives"),indent=4))
    get_directory_file_counts("/mnt/InternalStorage/sidkas/project_ks_protocoll/new_downloaded_pdfs")