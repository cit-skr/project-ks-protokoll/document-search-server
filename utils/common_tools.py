""" App configuration. """
import os, sys, dotenv, datetime, traceback, errno, uuid
import io, requests, argparse, re, json, time
parent_folder_path = os.path.dirname( os.path.abspath(__file__)).split("document-search-server")[0]
sys.path.append(parent_folder_path)

# Find .env file
basedir = os.path.join(parent_folder_path,"document-search-server")
dotenv.load_dotenv(os.path.join(basedir, '.env'))

# General Config
NOW = datetime.datetime.now() 
FILE_DOWNLOAD_PATH = os.path.join(parent_folder_path, os.environ.get("FILE_DOWNLOAD_PATH"))
ELASTICSEARCH_HOST = os.environ.get("ELASTICSEARCH_HOST")
ELASTICSEARCH_PORT = os.environ.get("ELASTICSEARCH_PORT")
ELASTICSEARCH_INDEX_NAME = os.environ.get("ELASTICSEARCH_INDEX_NAME")
ELASTICSEARCH_CONFIG_PATH = os.path.join(basedir, "es_module/elasticsearch_config.json")
HASURA_URL = os.environ.get("HASURA_URL")
HASURA_ADMIN_PASSWORD = os.environ.get("HASURA_ADMIN_PASSWORD")
TOKENIZER_LANGUAGE = 'swedish' # 'english'

from tqdm import tqdm
# from pdfminer import high_level

import numpy as np
import pandas as pd

import elasticsearch
from elasticsearch import Elasticsearch

import nltk
nltk.download('punkt')
from nltk.tokenize import sent_tokenize
from gql import gql, Client, AIOHTTPTransport, transport
import gql as gql_main

from datetime import datetime
from dateutil.tz import gettz
def now():  
    dt = datetime.now(gettz("Europe/Stockholm")).strftime('%Y-%m-%d %H-%M-%S.%d')[:-3]
    return str(dt.replace(' ','_'))


## Hasura GrahQL
def get_gql_client():
    global HASURA_URL
    global HASURA_ADMIN_PASSWORD
    transport = AIOHTTPTransport(
        url= HASURA_URL,
        headers={'Content-Type': 'application/json', 'x-hasura-admin-secret': HASURA_ADMIN_PASSWORD }
    )
    # Create a GraphQL client using the defined transport
    client = Client(transport=transport, fetch_schema_from_transport=True)
    return client


def execute_gql(client, query): # Note: use execute gql everywhere instead of calling gql directly
    r = None
    try:
        r = client.execute(gql(query))
        return r
    except gql_main.transport.exceptions.TransportQueryError as e:
        if "Uniqueness violation" not in str(e):
            print(e)
            traceback.print_exc()
        return r
    except BaseException as e:
        print(e)
        return r


## ElasticSearch

def get_es_client():
    global ELASTICSEARCH_HOST, ELASTICSEARCH_PORT
    # Connect to the elastic cluster 
    client = Elasticsearch([{'host':ELASTICSEARCH_HOST,'port':ELASTICSEARCH_PORT}])
    return client

# es document template
def create_doc(file_id=0,municipality_id=0,municipality_name="",county_name="",file_path="",title="",summary="",size="",page_count=0,date="",url="",text=[]):
    doc = {
        '_op_type': 'index',
        'file_id': file_id,
        'municipality_id': municipality_id,
        'municipality_name': municipality_name,
        "county_name":county_name,
        "path":file_path,
        'title': title,
        'summary':summary,
        'size': size,
        'page_count':page_count,
        'date': date,
        "url":url,
        'text': text
    }
    return doc



def store_record(client, index_name, record, _id):
    if _id == None or _id == 0:
        _id = str(uuid.uuid1())
    try:
        outcome = client.index(index=index_name, body=record,id=str(_id))
    except Exception as ex:
        print('Error in indexing data')
        print(str(ex))


##  file handling

def mkdir_p(path, folder_name = None):
    if folder_name:
        abs_folder_path = os.path.join(path, folder_name)
    else:
        abs_folder_path = path
    try:
        os.makedirs(abs_folder_path)
    except OSError as exc:
        if exc.errno == errno.EEXIST and os.path.isdir(abs_folder_path):
            pass
        else:
            raise

def sizeof_fmt(num, suffix='B'):
    for unit in ['','K','M','G','T','P','E','Z']:
        if abs(num) < 1024.0:
            return "%3.1f%s%s" % (num, unit, suffix)
        num /= 1024.0
    return "%.1f%s%s" % (num, 'Yi', suffix)

def get_file_paths(dir_path, filter=".txt"):
    file_paths = []
    for root, dirs, files in os.walk(dir_path):
        for _file in files:
            if _file.endswith(filter):
                file_path = os.path.join(root, _file)
                file_size = os.path.getsize(file_path)
                createDate = time.ctime(os.path.getctime(file_path))
                file_paths.append((file_path, createDate, file_size))
    return file_paths

from pdfminer.pdfparser import PDFParser
from pdfminer.pdfdocument import PDFDocument
from pdfminer.pdfpage import PDFPage
from pdfminer.pdfinterp import resolve1

from PIL import Image 
import pytesseract 
from pdf2image import convert_from_path, convert_from_bytes

def extract_pdf(_file):
    is_content_bytes = type(_file)==bytes
    if is_content_bytes:
        stream = io.BytesIO(_file)
    elif os.path.isfile(_file):
        stream = open(_file,'rb')
    else:
        return "", 0
       
    text = high_level.extract_text(stream)

    parser = PDFParser(stream)
    document = PDFDocument(parser)
    page_count = resolve1(document.catalog['Pages'])['Count']
    if len(text) < 300 and page_count >3:
        text = ""; page_count = 0
        pages =  convert_from_bytes(_file,500) if is_content_bytes else convert_from_path(_file, 500) 
        for page in pages:
            try:
                text += str(((pytesseract.image_to_string(page)))) 
                page_count += 1
            except BaseException as e:
                print(e)
                traceback.print_exc()
     
    return text, page_count


import re
alphabets= "([A-Za-z])"
prefixes = "(Mr|St|Mrs|Ms|Dr)[.]"
suffixes = "(Inc|Ltd|Jr|Sr|Co)"
starters = "(Mr|Mrs|Ms|Dr|He\s|She\s|It\s|They\s|Their\s|Our\s|We\s|But\s|However\s|That\s|This\s|Wherever)"
acronyms = "([A-Z][.][A-Z][.](?:[A-Z][.])?)"
websites = "[.](com|net|org|io|gov)"

def split_into_sentences(text):
    text = " " + text + "  "
    text = text.replace("\n"," ")
    text = re.sub(prefixes,"\\1<prd>",text)
    text = re.sub(websites,"<prd>\\1",text)
    if "Ph.D" in text: text = text.replace("Ph.D.","Ph<prd>D<prd>")
    text = re.sub("\s" + alphabets + "[.] "," \\1<prd> ",text)
    text = re.sub(acronyms+" "+starters,"\\1<stop> \\2",text)
    text = re.sub(alphabets + "[.]" + alphabets + "[.]" + alphabets + "[.]","\\1<prd>\\2<prd>\\3<prd>",text)
    text = re.sub(alphabets + "[.]" + alphabets + "[.]","\\1<prd>\\2<prd>",text)
    text = re.sub(" "+suffixes+"[.] "+starters," \\1<stop> \\2",text)
    text = re.sub(" "+suffixes+"[.]"," \\1<prd>",text)
    text = re.sub(" " + alphabets + "[.]"," \\1<prd>",text)
    if "”" in text: text = text.replace(".”","”.")
    if "\"" in text: text = text.replace(".\"","\".")
    if "!" in text: text = text.replace("!\"","\"!")
    if "?" in text: text = text.replace("?\"","\"?")
    text = text.replace(".",".<stop>")
    text = text.replace("?","?<stop>")
    text = text.replace("!","!<stop>")
    text = text.replace("<prd>",".")
    sentences = text.split("<stop>")
    sentences = sentences[:-1]
    sentences = [s.strip() for s in sentences]
    return sentences

def load_textfile(path):
    file_input = []
    with open(path,'r') as f:
        for l in f.readlines():
            if len(l.replace('\n',''))>15:
                sents = split_into_sentences(l) #,language=TOKENIZER_LANGUAGE)
          
                for sent in sents:
                    file_input.append(sent)
    return file_input



## Text cleanup
clean_line = lambda line: re.sub(r"[^a-zA-ZäöåÄÖÅ0-9,;?!'-]+",' ', line.strip())  # Note: check if " . " is needed in the regexp
def clean_text(text):
    global TOKENIZER_LANGUAGE
    sents = sent_tokenize(text,language=TOKENIZER_LANGUAGE)
    cleaned_sents = [clean_line(item) for item in sents]
    text_sents = [ l for l in cleaned_sents if len(l)>20]
    return text_sents

import datetime
find_full_date = re.compile(r'.*(20[0-9]{2}?[- /]?[0-3]?[0-9][- /]?(?:[0-9]{2})?[0-9]{2})')
find_md = re.compile(r'.*((20[1-2][0-9])[- /]?(0[1-9]|1[0-2])[- /]?([0-2][0-9]|3[0-1]))')
def get_meeting_date(text):
    dates = [dt[0] for dt in find_full_date.findall(text.replace("\\",""))]
    dates += [dt[0] for dt in find_md.findall(text.replace("\\",""))]
    filtered_dates = {}
    for date in dates:
        if '-' in date:
            split_date = date.split('-')
            if len(split_date) >2:
                try:
                    dt = datetime.datetime.strptime(date,'%Y-%m-%d')
                    if NOW >= dt: 
                        filtered_dates[date] = dt
                except BaseException as e:
                    pass
    if len(filtered_dates)>0:
        date_list = sorted(list(filtered_dates.values()),reverse=True)
        return date_list[0]
    else:
        return datetime.datetime.strptime("1990-01-01",'%Y-%m-%d') 
    

def get_meeting_info(text):
    info = ""
    lines = [l for l in text.splitlines() if len(l)>1]
    for i, line in enumerate(lines):
        if ' kl.' in line.lower():
            if i>0: info+=lines[i-1] + '\n'
            info+= line + '\n'+ lines[i+1]
            break
    return clean_line(info)




## URL handling

from collections import OrderedDict
def clean_pdf_url(url):
    cleaned_url = None
    try:
        split_url = str(url).split('//')
        cleaned_url = split_url[0]+ '//' + '/'.join(OrderedDict.fromkeys(split_url[1].split('/')))
    except BaseException:
        print(url)
        traceback.print_exc()
    return cleaned_url

def clean_path(path):
    _path = str(path)
    try:
        return '/'.join(OrderedDict.fromkeys(_path.split('/')))
    except:
        return _path


# Note: check the regular expression for repeated patterns
find_year = re.compile(r'.*(20[1-2][0-9])')
find_month = re.compile(r'.*(jan(?:uari)?|feb(?:ruari)?|mar(?:s)?|apr(?:il)?|may|jun(?:i)?|jul(?:i)?|aug(?:usti)?|sep(?:tember)?|okt(?:ober)?|(nov|dec)(?:ember)?)')
find_full_date = re.compile(r'.*(20[0-9]{2}?[- /]?[0-3]?[0-9][- /]?([0-9]{2})?[0-9]{2})')
find_md = re.compile(r'.*((20[1-2][0-9])[- /]?(0[1-9]|1[0-2])[- /]?([0-2][0-9]|3[0-1]))')

def get_date(string):
    y = find_year.findall(string)
    m = find_month.findall(string)
    ymd = find_full_date.findall(string)
    md = find_md.findall(string)
    date_extract = {
        'y': y[0] if y else '', 
        'm': m[0][0] if m else '', 
        'md': md[0][0] if md else '', 
        'ymd': ymd[0][0] if ymd else ''
    }
    return date_extract



if __name__=='__main__':
    path = "/mnt/InternalStorage/sidkas/project_ks_protocoll/document-search-server/utils/2020-06-10.pdf"
    url = "https://buildmedia.readthedocs.org/media/pdf/pdfminer-docs/latest/pdfminer-docs.pdf"

    # ocr_sample = "/mnt/InternalStorage/sidkas/project_ks_protocoll/new_downloaded_pdfs/Dalarnas län/Älvdalen/1990/71969 - 1990-01-01.pdf"
    # r = requests.get(url, verify=False)
    # a,b = extract_pdf(ocr_sample)
    # print(a,b)

    # extract_pdf_ocr(ocr_sample)

    txt_file = "/mnt/InternalStorage/sidkas/project_ks_protocoll/new_downloaded_pdfs/Stockholms län/Salem/2020/11 - 2020-04-03.pdf"

    text1, _ = extract_pdf(txt_file)
    text = clean_text(text1)

    print(text)
    