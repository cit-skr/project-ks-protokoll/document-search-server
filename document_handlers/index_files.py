""" App configuration. """
import os, sys, dotenv, datetime, traceback, re, json, time, argparse, multiprocessing
from tqdm import tqdm
import numpy as np
parent_folder_path = os.path.dirname( os.path.abspath(__file__)).split("document-search-server")[0]


# Find .env file
basedir = os.path.join(parent_folder_path,"document-search-server")
sys.path.append(basedir)
dotenv.load_dotenv(os.path.join(basedir, '.env'))

# General Config
NOW = datetime.datetime.now() 
FILE_DOWNLOAD_PATH = os.path.join(parent_folder_path, os.environ.get("FILE_DOWNLOAD_PATH"))
ELASTICSEARCH_HOST = os.environ.get("ELASTICSEARCH_HOST")
ELASTICSEARCH_PORT = os.environ.get("ELASTICSEARCH_PORT")
ELASTICSEARCH_INDEX_NAME = os.environ.get("ELASTICSEARCH_INDEX_NAME")
ELASTICSEARCH_CONFIG_PATH = os.path.join(basedir, "es_module/elasticsearch_config.json")
HASURA_URL = os.environ.get("HASURA_URL")
HASURA_ADMIN_PASSWORD = os.environ.get("HASURA_ADMIN_PASSWORD")
TOKENIZER_LANGUAGE = 'swedish' # 'english'
MUNICIPALITY_INFO_FROM_ID = None

""" Script to create elasticsearch documents by parsing txt files. """

import elasticsearch
from elasticsearch import Elasticsearch

from gql import gql, Client

from utils.common_tools import get_gql_client, get_es_client, get_date,clean_pdf_url, load_textfile, create_doc, store_record



## Hasura GQL queries
get_file_info = """
    query download_success{
        ks_protokoll_schema_file_info(where: {download_status: {_eq: "success"}}) {
            file_id
            meeting_date
            meeting_info
            relative_file_path
            pdf_url
            url_text
            page_count
            municipality_id
            content_size
            is_indexed
        }
    }
"""

get_municipality_info = """
    query mun_info {
        ks_protokoll_schema_municipality_info {
            municipality_id
            county_name
            name
        }
    }
"""

set_index_status = """
    mutation set_index_status {
        update_ks_protokoll_schema_file_info_by_pk(pk_columns: {file_id: %d}, _set: {is_indexed: "yes"}) {
            is_indexed
        }
    }
"""

set_meeting_date = """
    mutation set_meeting_date {
        update_ks_protokoll_schema_file_info_by_pk(pk_columns: {file_id: %d}, _set: {meeting_date: "%s"}) {
            meeting_date
        }
    }
"""


insert_file_info = """
    mutation {
        insert_ks_protokoll_schema_file_info_one(object: {download_status: "success", content_type: "application/pdf", content_size: %d, is_indexed: "yes", page_count: %d, relative_file_path: "%s", status_code: 200}) {
            file_id
        }
    }
"""


def set_municipality_info():
    global MUNICIPALITY_INFO_FROM_ID
    gql_client = get_gql_client()
    mun_info = gql_client.execute(gql(get_municipality_info))["ks_protokoll_schema_municipality_info"]
    MUNICIPALITY_INFO_FROM_ID = {item.get("municipality_id"):item for item in mun_info}


def create_index():
    global ELASTICSEARCH_CONFIG_PATH, ELASTICSEARCH_INDEX_NAME
    client = get_es_client()
    client.indices.delete(index=ELASTICSEARCH_INDEX_NAME, ignore=[404])
    with open(ELASTICSEARCH_CONFIG_PATH) as f:
        source = f.read().strip()
        client.indices.create(index=ELASTICSEARCH_INDEX_NAME, body=source)

# Note: remove all meta.jsonl files, they are redundant

def load_meta_data():      
    global FILE_DOWNLOAD_PATH, NOW, MUNICIPALITY_INFO_FROM_ID
    gql_client = get_gql_client()
    index_que = []
    file_info = gql_client.execute(gql(get_file_info))["ks_protokoll_schema_file_info"]
    # pbar = tqdm(total=len(file_info))
    for file_item in file_info:
        pdf_file_path = file_item.get("relative_file_path").replace("('", '').replace("',)",'') # Note: fix this! get proper relative path
        relative_pdf_file_path = pdf_file_path.replace(parent_folder_path,'').split('/',maxsplit=1)[1]
        pdf_file_path = os.path.join(FILE_DOWNLOAD_PATH,relative_pdf_file_path)
        text_file_path = pdf_file_path.replace('.pdf', '.txt')
        page_count = int(file_item.get("page_count"))
        file_id = int(file_item.get("file_id"))
        if page_count>3:
            meeting_date = file_item.get("meeting_date")
            year =  int(meeting_date.split('-')[0])  if meeting_date  else 1990
            url_text = file_item.get("url_text")
            meeting_info = file_item.get("meeting_info")
            summary = url_text if url_text else "" + '\n' + meeting_info if meeting_info else ""
            date_info = get_date(summary)
            pdf_url = file_item.get("pdf_url")
            cleaned_date_info = []
            if pdf_url:
                all_dates = get_date(summary+' '.join((pdf_url.split('/')[-3])))
                cleaned_date_info = list({item.replace('/','-') for item in all_dates.values() if len(item)>9})
                

            check_dates =  None 
            if len(cleaned_date_info) >0:
                dt = cleaned_date_info[-1]
                if dt.find('-')==2 and (cleaned_date_info[-1] != meeting_date):
                    check_dates = True
                

            if (year == 1990 or year > NOW.year) or check_dates:
                if check_dates is not None :
                    try:
                        gql_client.execute(gql(set_meeting_date % (file_id, cleaned_date_info[-1])))
                        meeting_date = cleaned_date_info[-1]
                    except BaseException as e:
                        print(e)
                        print(meeting_date, pdf_file_path)
                        traceback.print_exc()

            title = str(file_id) + ' - ' + ":".join(list({item for item in date_info.values() if len(item)>1}))
            municipality_id = int(file_item.get("municipality_id")) if pdf_url else 291
            doc = create_doc(
                file_id = file_id,
                municipality_id = municipality_id,
                municipality_name= MUNICIPALITY_INFO_FROM_ID[municipality_id].get("name") if MUNICIPALITY_INFO_FROM_ID else "",
                county_name= MUNICIPALITY_INFO_FROM_ID[municipality_id].get("county_name") if MUNICIPALITY_INFO_FROM_ID else "",
                file_path = relative_pdf_file_path,
                title = title, 
                summary = summary,
                size = int(file_item.get("content_size")) if pdf_url else 0,
                page_count = page_count,
                date = meeting_date,
                url = clean_pdf_url(file_item.get("pdf_url")) if pdf_url else 0,
            )
            index_que.append((doc,text_file_path,pdf_file_path))
            # pbar.update()

    return index_que

def parse_index_sub_func(doc,pdf_file_path):
    gql_client = get_gql_client()
    es_client = get_es_client()
    try:
        file_id = int(doc.get("file_id"))
        text1, _ = extract_pdf(pdf_file_path)
        text = clean_text(text1)
        # text = load_textfile(text_file_path)
        # print(doc)
        doc.update({"text": text})

        store_record(es_client,ELASTICSEARCH_INDEX_NAME,doc, file_id)

        gql_client.execute(gql(set_index_status % (file_id)))

    except BaseException as e:
        print(e)
        traceback.print_exc()
                
def parse_and_index_text_files():
    global ELASTICSEARCH_INDEX_NAME
    index_que = load_meta_data()
    pbar = tqdm(total=len(index_que))

    step_size = 10
    max_len = len(index_que)
    iters = zip(list(np.arange(0,max_len,step_size)),list(np.arange(step_size,max_len,step_size)) + [max_len])

    for i,j in iters:
        items = index_que[i:j]
        threads = []
        for doc, text_file_path, pdf_file_path in items:
            thread = multiprocessing.Process(target=parse_index_sub_func,args=(doc,pdf_file_path))
            threads.append(thread)
            thread.start()
            # p.map(async_download,(item,))
        
        for th in threads:
            th.join()
        pbar.update(step_size)
        
        




from utils.common_tools import extract_pdf, get_meeting_date, get_meeting_info, sizeof_fmt, clean_text

async def parse_and_index_pdf_file(pdf_path, file_id=None):
    global ELASTICSEARCH_INDEX_NAME, FILE_DOWNLOAD_PATH
    try:
        gql_client = get_gql_client()
        es_client = get_es_client()
        
        text, page_count = extract_pdf(pdf_path)

        cleaned_text = clean_text(text)
        meeting_info = get_meeting_info('\n'.join(cleaned_text))

        date = get_meeting_date(pdf_path+ ' ' +meeting_info + cleaned_text)

        relative_file_path = pdf_path.replace(FILE_DOWNLOAD_PATH,'')
        size = os.path.getsize(pdf_path)

        if file_id == None:
            try:
                r = await gql_client.execute_async(gql(insert_file_info % (size, page_count,pdf_path)))
                file_id = r["insert_ks_protokoll_schema_file_info_one"].get('file_id')
            except:
                traceback.print_exc()

        doc = create_doc(
            file_id = file_id if file_id else 0 ,
            file_path = relative_file_path,
            summary = meeting_info,
            size =  int(size), 
            page_count = page_count,
            date = str(date).split(' ')[0],
            text=cleaned_text
        )

        store_record(es_client,ELASTICSEARCH_INDEX_NAME,doc,file_id)

        

        with open("stored_docs.jsonl",'a+') as stored_file:
            store = create_doc(
                file_id = file_id if file_id else 0,
                file_path = relative_file_path,
                summary = meeting_info,
                size =  size, 
                page_count = page_count,
                date = date
            )
            stored_file.write(json.dumps(store)+"\n")

        return True
    except BaseException:
        traceback.print_exc()
        return False



if __name__=='__main__':
    # create_index()
    set_municipality_info()
    parse_and_index_text_files()