""" App configuration. """
import os, sys, dotenv, datetime, traceback, errno, io, re, json, requests, time, threading, asyncio

from gql import gql, Client
from collections import OrderedDict
from tqdm import tqdm
import numpy as np
import urllib3, multiprocessing
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

parent_folder_path = os.path.dirname( os.path.abspath(__file__)).split("document-search-server")[0]
# Find .env file
basedir = os.path.join(parent_folder_path,"document-search-server")
dotenv.load_dotenv(os.path.join(basedir, '.env'))
sys.path.append(basedir)

from utils.common_tools import clean_line, extract_pdf, mkdir_p, clean_text, get_gql_client, clean_pdf_url, get_meeting_date, get_meeting_info, clean_path, get_date


# General Config
NOW = lambda: datetime.datetime.now() 
FILE_DOWNLOAD_PATH =  os.path.join(parent_folder_path, os.environ.get("FILE_DOWNLOAD_PATH"))
HASURA_URL = os.environ.get("HASURA_URL")
HASURA_ADMIN_PASSWORD = os.environ.get("HASURA_ADMIN_PASSWORD")
TOKENIZER_LANGUAGE = 'swedish' # 'english'

mkdir_p(FILE_DOWNLOAD_PATH)

## hasura gql queries
get_municipality_info = """
    query MyQuery {
        ks_protokoll_schema_municipality_info(order_by: {n_pdfs: desc}) {
            municipality_id
            county_name
            name
            n_pdfs
        }   
    }
"""

get_file_info = """
    query get_data($_eq: Int = %d) {
        ks_protokoll_schema_file_info(where: {municipality_id: {_eq: $_eq}}) {
            pdf_url
            url_text
            webpage_url
            file_id
            download_status
            content_type
        }
    }

"""

update_file_info_query = """
    mutation update_usage($file_id: Int! = %d, $status_code: Int! = %d, $content_type: String = "%s", $content_size: Int! = %d, $download_status: String = "%s", $relative_file_path: String = "%s", $meeting_date: date = "%s", $meeting_info: String = "%s", $page_count: Int = %d) {
        update_ks_protokoll_schema_file_info_by_pk(pk_columns: {file_id: $file_id}, _set: {status_code: $status_code, content_type: $content_type, content_size: $content_size, download_status: $download_status, relative_file_path: $relative_file_path, meeting_date: $meeting_date, meeting_info: $meeting_info, page_count: $page_count}) {
            status_code
            content_type
            content_size
            download_status
            relative_file_path
            meeting_date
            meeting_info
            page_count
        }
    }

"""

update_file_info_status = """
    mutation update_usage($file_id: Int! = %d, $download_status: String = "%s") {
        update_ks_protokoll_schema_file_info_by_pk(pk_columns: {file_id: $file_id}, _set: {download_status: $download_status}) {
            download_status
        }
    }

"""


insert_file_info = """
     mutation file_insert {
        insert_ks_protokoll_schema_file_info_one(object: {pdf_url: "%s", content_type: "application/pdf", is_indexed: "no", download_status: "todo", municipality_id: %d}) {
            file_id
        }
    }
"""


def get_file_id(pdf_url,m_id):
    m_id = m_id if m_id else 0
    gql_client = get_gql_client()
    file_id = 0
    try:
        r = gql_client.execute(gql(insert_file_info % (str(pdf_url), int(m_id))))
        file_id = r["insert_ks_protokoll_schema_file_info_one"].get("file_id")
        return file_id
    except BaseException:
        traceback.print_exc()
        return file_id



def download_url(url, path, file_id, url_text=""):
    global FILE_DOWNLOAD_PATH
    folder_path = clean_path(os.path.join(FILE_DOWNLOAD_PATH, path))

    # defaults
    status_code = 404
    content_type = ""
    content_length = -1
    r = None
    try:
        download_status = "failed"
        r = requests.get(url, verify=False)
        status_code = int(r.status_code)
        content_type = r.headers.get('content-type',default="")
        content_length = int(r.headers.get('content-length',default=-1))
    except BaseException as e:
        print(e)
        traceback.print_exc()
        

    file_path = str(folder_path)
    date_string = "1990-01-01"
    meeting_info = ""
    page_count = 0
   

    if status_code <300 and content_type == "application/pdf":
        try: 
            text, page_count = extract_pdf(r.content)
            meeting_date = get_meeting_date(url_text + text)

            date_string = str(meeting_date).split(' ')[0]

            if str(meeting_date.year) not in folder_path:
                year_dir = os.path.join(folder_path,str(meeting_date.year))
                mkdir_p(path= year_dir)
                folder_path = year_dir
            # if str(m) not in folder_path:
            #     month_dir = os.path.join(year_dir,str(m))
            #     mkdir_p(path= month_dir)
            #     folder_path = month_dir

            file_path = os.path.join(folder_path, f"{file_id} - {date_string}.pdf")
            text_file_path = os.path.join(folder_path, f"{file_id} - {date_string}.txt")

            with open(file_path, 'wb') as f:
                f.write(r.content)

            with open(text_file_path, 'w') as f:
                text_sentences = clean_text(text)
                for sent in text_sentences:
                    f.write(sent + "\n")

            meeting_info = get_meeting_info(text)
            download_status = "success"
        except BaseException as e:
            print(NOW())
            download_status = "error -- " + str(e)
            traceback.print_exc()
            print(file_id, url)
            print("-----------------------")
    else:
        # print(NOW())
        # print(content_type,content_length)
        download_status = "error"
        # print(file_id, url)
        # print("-----------------------")

    insert_data = [
            status_code, content_type, content_length, 
            download_status, file_path, date_string, meeting_info, page_count
    ]
    data_dict = {
        "file_path":file_path,
        "status_code":status_code,
        "content_type":content_type,
        "content_length":content_length,
        "download_status":download_status,
        "page_count":page_count,
        "meeting_date":date_string,
        "url":url,
        "meeting_info":meeting_info,
    }
    if download_status == "success":
        # Manually to keep track of what's being downloaded
        with open("downloaded_files.jsonl",'a+') as jl:
            jl.write(json.dumps(data_dict)+'\n')
    
    return insert_data, data_dict
   
    

def update_file_info(insert_data, file_id):
    gql_client = get_gql_client()
    try:
        response = gql_client.execute(gql(update_file_info_query % (file_id, *insert_data)))
        out = response["update_ks_protokoll_schema_file_info_by_pk"]
        out.update({"file_id": file_id})
        # writer = open(os.path.join(folder_path,"meta.jsonl"), 'a+') 
        # writer.write(json.dumps(out) + '\n')
        # writer.close()

    except BaseException as e:
        print(NOW())
        print(e)
        traceback.print_exc()
        print(file_id)
        print("-----------------------")


async def download_pdfs_for_each_municipality(item):
    try:
        gql_client = get_gql_client()
        county_dir = os.path.join(FILE_DOWNLOAD_PATH,item.get("county_name"))
        municipality_dir = os.path.join(county_dir,item.get("name"))
        municipality_id = item.get("municipality_id")
        n_pdfs = item.get("n_pdfs") if item.get("n_pdfs")  != None else 0 

        ## Make directory for the municipality 
        ## ex: county/municiplaity/year/month/date.pdf
        mkdir_p(path=county_dir)
        mkdir_p(path=municipality_dir)

        ## Download all pdf urls
        if n_pdfs>0:
            file_items = await gql_client.execute_async(gql(get_file_info % (municipality_id)))
            pbar = tqdm(total=len(file_items["ks_protokoll_schema_file_info"]))
            for file_item in file_items["ks_protokoll_schema_file_info"]:
                pdf_url = file_item.get("pdf_url", None)
                file_id = file_item["file_id"]
                content_type = file_item.get("content_type", "")
                if content_type is None: content_type = ""
                download_status = file_item.get("download_status", "")
                url_text = file_item.get("url_text") if file_item.get("url_text") else ""

                if ("html" not in content_type): # and ("success" not in download_status):  # Note: uncomment this after trial
                    cleaned_pdf_url = clean_pdf_url(pdf_url)

                    if cleaned_pdf_url is not None: 
                        if "none" not in cleaned_pdf_url.lower():
                            insert_data,_ = download_url(cleaned_pdf_url,municipality_dir,file_id, url_text=url_text)
                            # response = await gql_client.execute_async(gql(update_file_info_query % (file_id, *insert_data)))
                    else:
                        download_status = "bad url"
                        # response = await gql_client.execute_async(gql(update_file_info_status % (file_id,download_status)))
                        print(file_id, cleaned_pdf_url)
                        print(download_status)
                        print("-----------------------")
            

                pbar.update()
    except BaseException as e:
        traceback.print_exc()

def async_download(item):
    asyncio.set_event_loop(asyncio.SelectorEventLoop())
    asyncio.get_event_loop().run_until_complete(download_pdfs_for_each_municipality(item))

### main method

def main():
    global FILE_DOWNLOAD_PATH
    gql_client = get_gql_client()
    municipality_data = gql_client.execute(gql(get_municipality_info))

    m_data = municipality_data["ks_protokoll_schema_municipality_info"]
    step_size = 5
    iters = zip(list(np.arange(0,len(m_data),step_size)),list(np.arange(step_size,len(m_data),step_size)) + [len(m_data)])

    for i,j in iters:
        items = m_data[i:j]
        threads = []
        for item in items:
            thread = multiprocessing.Process(target=async_download,args=(item,))
            threads.append(thread)
            thread.start()
            # p.map(async_download,(item,))
        
        for th in threads:
            th.join()

def test():
    gql_client = get_gql_client()
    municipality_data = gql_client.execute(gql(get_municipality_info))    
    print(json.dumps(municipality_data["ks_protokoll_schema_municipality_info"][:5],indent=4))

from tqdm import tqdm
def update_status():
    gql_client = get_gql_client()
    with open( os.path.join(basedir , "document_handlers/downloaded_files.jsonl"),'r') as files:
        lines = files.readlines()
        pbar = tqdm(total=len(lines))
        for l in lines:
            line = json.loads(l)
            insert_data = [
                    line.get("status_code"),
                    line.get("content_type"), 
                    line.get("content_length"), 
                    line.get("download_status"), 
                    line.get("file_path"), 
                    line.get("meeting_date"), 
                    line.get("meeting_info"), 
                    line.get("page_count")
            ]

            file_id = int(line.get("file_path").split('/')[-1].split(' - ')[0])
            try:
                response = gql_client.execute(gql(update_file_info_query % (file_id, *insert_data)))
                pbar.update()
            except BaseException as e:
                traceback.print_exc()


            


if __name__=='__main__':
    # main()
    update_status()






