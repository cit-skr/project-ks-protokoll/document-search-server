""" App configuration. """
import os, sys, dotenv, datetime, traceback, threading, json, asyncio, datetime
parent_folder_path = os.path.dirname( os.path.abspath(__file__)).split("document-search-server")[0]
sys.path.append(parent_folder_path)

# Find .env file
basedir = os.path.join(parent_folder_path,"document-search-server")
dotenv.load_dotenv(os.path.join(basedir, '.env'))

# General Config
NOW = datetime.datetime.now() 
FILE_DOWNLOAD_PATH = os.path.join(parent_folder_path, os.environ.get("FILE_DOWNLOAD_PATH"))
ELASTICSEARCH_INDEX_NAME = os.environ.get("ELASTICSEARCH_INDEX_NAME")
ELASTICSEARCH_nHits = os.environ.get("ELASTICSEARCH_nHits")
ELASTICSEARCH_nResults = os.environ.get("ELASTICSEARCH_nResults")

import time
import numpy as np
import pandas as pd
from flask import Flask, request, send_file
from es_module.es_query import get_es_client, search, delete_document
from utils.index_directory import get_directory_structure
from utils.file_handler import delete_file, rename_file_folder, create_new_folder
from document_handlers.index_files import parse_and_index_pdf_file

es_client = get_es_client()
app = Flask(__name__)


def save_search_query(text):
    iso_timestamp = datetime.datetime.now().isoformat()
    with open("search_history.csv",'a+') as handler:
        handler.write(f"{iso_timestamp},{text}\n")

@app.route('/api/elasticsearch/next_sentence', methods = ['POST'])
def get_next_sentence():
    input_text = request.get_json()
    search_results, suggestions, status = search(es_client, input_text['input'], suggest=True)
    # print(search_results)
    return {'search_results':search_results, 'suggestions':[str(s['text']).strip() for s in suggestions], "status":status }


@app.route('/api/elasticsearch/query', methods = ['POST'])
def search_docs():
    input_text = request.get_json()
    save_search_query(input_text['input'])
    search_results, suggestions, status = search(es_client, 
        input_text['input'],
        max_hits=input_text['nHits'],
        max_search_results=input_text['nResults'],
    )
    # print(search_results)
    return {'search_results':search_results, 'suggestions':suggestions, "status":status }

def async_parse_index_pdf_file(save_path,file_id=None):
    asyncio.set_event_loop(asyncio.SelectorEventLoop())
    asyncio.get_event_loop().run_until_complete(parse_and_index_pdf_file(save_path,file_id))


@app.route('/api/upload/files', methods= ['POST'])
def file_upload():
    status = ""
    if request.files:
        print(request.files)
        for k, v in request.files.items():
            print(k)
            ks = k.split(' ',maxsplit=1)
            save_path = os.path.join(ks[1],  ks[0] + ' '+ v.filename)
            v.save(save_path)
            thread = threading.Thread(target=async_parse_index_pdf_file, args=(save_path,))
            thread.start()
        status = "success"
    else:
        status = "error!"
        print("No files found!")

    return {"status": status}

from document_handlers.download_urls import get_file_id, download_url, update_file_info
from document_handlers.index_files import parse_and_index_pdf_file

def download_parse_index(url, path, m_id):
    file_id = get_file_id(url,m_id)
    data, data_dict = download_url(url,path,file_id)
    update_file_info(data, file_id)
    parse_and_index_pdf_file(data_dict.get("file_path"),file_id)




@app.route('/api/upload/url', methods= ['POST'])
def url_upload():
    data = request.get_json()
    path = data.get("path")
    url = data.get("url")
    m_id = data.get("m_id")
    m_id = m_id if m_id else 291
    thread = threading.Thread(target=download_parse_index, args=(url,path,m_id))
    thread.start()

    print(data)
    return {"status":"success"}

@app.route('/api/folder_structure', methods= ['GET'])
def get_fs():
    directories =  get_directory_structure(FILE_DOWNLOAD_PATH) 
    return {"dirs": directories}




@app.route('/api/download', methods=['POST', 'GET'])
def file_download():
    path = request.args.get("path")
    return send_file(path,as_attachment=True)

@app.route('/api/delete', methods=['POST', 'GET'])
def file_delete():
    data = request.get_json()
    return_code, response = delete_document(es_client, data.get("path"), data.get("size"))
    status = delete_file(data.get("path"))
    if return_code == None:  
        status.update({"status":str(response)})
    else: 
        json.dump(response, open("delete_response.json",'w'), indent=4)
    return status

@app.route('/api/rename', methods=['POST', 'GET'])
def rename():
    data = request.get_json()
    status = rename_file_folder(data.get("path"),data.get("name"))
    return status

@app.route('/api/create_folder', methods=['POST', 'GET'])
def create_folder():
    data = request.get_json()
    status = create_new_folder(data.get("path"),data.get("name"))
    return status

PROGRESS  = 0
import os, time
def progress(callback):
    val = 0
    for i in range(100):
        time.sleep(1)
        callback(i)
        val = i
    return val

def set_progress(x):
    global PROGRESS
    PROGRESS = x

@app.route('/api/progress')
def get_progress():
    global PROGRESS
    return {'progress':PROGRESS}

@app.route('/api/start_progress')
def start_progress():
    p = progress(set_progress)
    return {'progress':p}

def load_search_history():
    search_history_path = os.path.join(basedir,"search_history.csv")
    search_history_df = pd.read_csv(search_history_path, names=["ts","query"])
    print(len(search_history_df))
    search_history_df = search_history_df.drop_duplicates(subset=['query'], keep=False)
    print(len(search_history_df))
    print(search_history_df)

if __name__=='__main__':
    # path = "downloaded_pdfs/Östergötlands län"
    # url = "https://arxiv.org/pdf/1601.00013.pdf"
    # download_parse_index(url,path,291)
    load_search_history()